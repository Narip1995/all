import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";

// reactstrap components
import {Card,CardHeader,CardBody,CardTitle,Row,Col} from "reactstrap";

// core components
import {chartExample4} from "variables/charts.jsx";

class Dashboard extends React.Component {
  state = {
      bigChartData: "data1"
    };

  setBgChartData = name => {
    this.setState({
      bigChartData: name
    });
  };
  render() {
    return (
        <div className="content">
          <Row>
            <Col lg="4">
              <Card className="card-chart">
                <CardHeader>
                  <h5 className="card-category">Completed Tasks</h5>
                  <CardTitle tag="h3">
                    <i className="tim-icons icon-send text-success" /> 12,100K
                  </CardTitle>
                </CardHeader>
                <CardBody>
                  <div className="chart-area">
                    <Line data={chartExample4.data} options={chartExample4.options}/>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
    );
  }
}

export default Dashboard;
