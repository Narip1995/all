import React from 'react'
import { Card, CardHeader, CardBody, Row, Col, CardImg, Button, FormGroup, Label, Input, Collapse } from "reactstrap"
import database from '../../apis/config'
import { SketchPicker } from 'react-color'
import JsonShow from './JsonShow'

export default class JsonForm extends React.Component {


    state = {
        name: '',
        color: {},
        num: 0,
        isSelect: false,
        saved: {},
        displayColorPicker: false,
        loading: true,

    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({[name]: value})//no get ?

    }

    handleColorsChange = (color) => {
        this.setState({ color: color.rgb})
    }

    handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
      }

      handleClose = () => {
        this.setState({ displayColorPicker: false })
      }

      
    handleSave = async (event) => {
        event.preventDefault();
        const { name, color, num, isSelect, saved} = this.state;
        await this.setState({saved: {name, color, num, isSelect}});
        await this.writeConfigToBase(this.state.saved)
        this.ref.saveConfig();

    }

    writeConfigToBase = async (saved) => {
        const reponse = await database.post('/configuration', saved);
        console.log(reponse)
        this.setState({name:'',num:''})

    }

    
    render() {

        const { name, color, num, isSelect, saved, displayColorPicker, loading } = this.state;

        return (
            <div>

                <Card>

                    <CardHeader>
                        <h5 className="title">Configuration</h5>
                    </CardHeader>
                    
                    
                    <JsonShow ref={ins => {this.ref = ins}}/>

                    <CardBody className="all-icons">
                    <form onSubmit={this.handleSave}>
                      <div className="form-group">
                        <label htmlFor="name">Name :</label>
                        <input
                            type="text"
                            className="form-control"
                            name="name"
                            onChange={this.handleChange}
                            />
                      </div>
                      <div className="form-group">
                        <label htmlFor="num">Number :</label>
                        <input
                            type="number"
                            className="form-control"
                            name="num"
                            onChange={this.handleChange}/>

                      </div>
                      <div className="form-group d-inline-block mr-5"  onClick={ this.handleClick }>
                        <label className="btn" htmlFor="color">Color Picker</label>
                        { displayColorPicker ? <div>
                            <div onClick={ this.handleClose }/>
                            <SketchPicker color={ this.state.color } onChange={ this.handleColorsChange } />
                        </div> : null }
                      </div>
                      <FormGroup className="d-inline-block mr-5" check>
                        <Label check>
                              <Input
                                type="checkbox"
                                name="isSelect"
                                checked={isSelect}
                                onChange={this.handleChange} />
                              IsSelect
                        </Label>
                      </FormGroup>
                      <button type="submit" className="btn btn-danger" >Save</button>
                      </form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}