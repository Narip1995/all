import React from 'react'
import { Card, CardHeader, CardBody, Row, Col, CardImg, Button, FormGroup, Label, Input, Collapse } from "reactstrap"
import database from '../../apis/config'
import { SketchPicker } from 'react-color'


export default class JsonShow extends React.Component {
    state = {
        name: '',
        color: {},
        num: 0,
        isSelect: false,
        saved: {},
        displayColorPicker: false,
        loading: true,

    }

    toggle =()=>{
        this.setState(state => ({ collapse: !state.collapse }));
        this.fetchConfiguretion();
    }
    async fetchConfiguretion() {
        const response = await database.get('/configuration');
        const config = await response.data;
        console.log(config)
        await this.setState(prevState => ({
            name: config.name,
            color: {
                ...config.color
            },
            num: config.num,
            isSelect: config.isSelect,
            saved: { ...prevState.saved, ...config },
            loading: false
        }))
        console.log(this.state)
    }


    saveConfig =() => {
        
        this.fetchConfiguretion();
    }
   

    render() {
        console.log('render')
        const { name, color, num, isSelect, saved, displayColorPicker, loading } = this.state;

        return (
            <div>
                <Collapse isOpen={this.state.collapse}>
                    <Card>
                        <CardBody>
                            <h2>Show</h2>
                            <h5>Name : {name}</h5>                                              
                            <h5>Number : {num}</h5>
                            <h5>Color </h5>
                            <ul>
                                <li>r : {color.r}</li>
                                <li>g : {color.g}</li>
                                <li>b : {color.b}</li>
                                <li>a : {color.a}</li>
                            </ul>
                            <h5>IsSelect : {`${!loading && saved.isSelect}`}</h5>
                        </CardBody>
                    </Card>
                </Collapse>
                <Button color="primary" onClick={this.toggle} style={{ marginBottom: '1rem' }}>Toggle</Button>
            </div>
        );
    }
}