import React from 'react'
import { Card, CardHeader, CardBody, Row, Col, CardImg, Button, FormGroup, Label, Input, Collapse } from "reactstrap"
import database from '../../apis/config'
import { SketchPicker } from 'react-color'

// const ab=require('../../../../backend/db.json');;
export default class JsonForm extends React.Component {

    constructor() {
        super();
        this.state = {
            name: '',
            color: {},
            num: 0,
            isSelect: false,
            saved: {},
            displayColorPicker: false,
            loading: true,

        }
    }

    // componentDidMount() {
    //     this.fetchConfiguretion();
    // }

    toggle =()=>{
        this.setState(state => ({ collapse: !state.collapse }));
        this.fetchConfiguretion();
    }
    async fetchConfiguretion() {
        const response = await database.get('/configuration');
        const config = await response.data;
        // console.log(config)
        await this.setState(prevState => ({
            name: config.name,
            color: {
                ...config.color
            },
            num: config.num,
            isSelect: config.isSelect,
            saved: { ...prevState.saved, ...config },
            loading: false
        }))
        console.log(this.state)
    }

    handleChange = (event) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({[name]: value})//no get ?

    }

    handleColorsChange = (color) => {
        this.setState({ color: color.rgb})
    }

    handleClick = () => {
        this.setState({ displayColorPicker: !this.state.displayColorPicker })
      }

      handleClose = () => {
        this.setState({ displayColorPicker: false })
      }

      
    handleSave = async (event) => {
        event.preventDefault();
        const { name, color, num, isSelect, saved} = this.state;
        await this.setState({saved: {name, color, num, isSelect}});
        this.writeConfigToBase(this.state.saved)

    }

    writeConfigToBase = async (saved) => {
        const reponse = await database.post('/configuration', saved);
        console.log(reponse)

    }

    
    render() {

        const { name, color, num, isSelect, saved, displayColorPicker, loading } = this.state;

        return (
            <div>

                <Card>

                    <CardHeader>
                        <h5 className="title">Configuration</h5>
                    </CardHeader>

                    <div className="collapse" id="collapseExample">
                        <div className="card card-body">

                        </div>
                    </div>
                    
                    <Collapse isOpen={this.state.collapse}>
                        <Card>
                            <CardBody>
                                <h2>Show</h2>
                                <h5>Name : {name}</h5>
                                <h5>Number : {num}</h5>
                                <h5>Color </h5>
                                <ul>
                                    <li>r : {color.r}</li>
                                    <li>g : {color.g}</li>
                                    <li>b : {color.b}</li>
                                    <li>a : {color.a}</li>
                                </ul>
                                <h5>IsSelect : {`${!loading && saved.isSelect}`}</h5>
                            </CardBody>
                        </Card>
                    </Collapse>
                    <Button color="primary" onClick={this.toggle} style={{ marginBottom: '1rem' }}>Toggle</Button>
                    <hr />

                    <CardBody className="all-icons">
                    <form onSubmit={this.handleSave}>
                      <div className="form-group">
                        <label htmlFor="name">Name :</label>
                        <input type="text" className="form-control" name="name" onChange={this.handleChange}/>
                      </div>
                      <button type="submit" className="btn btn-danger">Save</button>
                    </form>
                    </CardBody>
                </Card>
            </div>
        );
    }
}