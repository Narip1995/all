import React from 'react';
import { Card, CardHeader, CardBody, Row, Col, CardImg, Button } from "reactstrap";

export default class LanguageForm extends React.Component {

    constructor() {
        super();
        this.state = {
          config: require('../../configuration.json').configuration,
          language: require('../../configuration.json').language,
          default: require('../../configuration.json').language.default
        };
       }

    render() {
        return(
            <div>
                <Card>
                    <CardHeader>
                      <h5 className="title">Language : {this.state.language[this.state.default].label}</h5>

                    </CardHeader>
                    <CardBody className="all-icons">
                      <h5>{this.state.language[this.state.default].windows.label} : </h5>
                      <ul>
                        <li>{this.state.language[this.state.default].windows.compass}</li>
                        <li>{this.state.language[this.state.default].windows.position}</li>
                        <li>{this.state.language[this.state.default].windows.error}</li>
                        <li>{this.state.language[this.state.default].windows.time}</li>
                      </ul>
                      <h5>{this.state.language[this.state.default].main_display.label} : </h5>
                      <ul>
                        <li>{this.state.language[this.state.default].main_display.trackData}</li>
                        <li>{this.state.language[this.state.default].main_display.trackList}</li>
                        <li>{this.state.language[this.state.default].main_display.trackInfo}</li>
                        <li>{this.state.language[this.state.default].main_display.displayOption}</li>
                      </ul>
                      <Button onClick={()=>{this.setState({default : "thai"})}}>
                        {this.state.language.thai.label}
                      </Button>
                      <Button onClick={()=>{this.setState({default : "english"})}} >
                        {this.state.language.english.label}
                      </Button>
                    </CardBody>
                </Card>
            </div>
        );
    }
}