import React from 'react';
import {Card, CardHeader, CardBody, Form, FormGroup, Label, Input } from 'reactstrap';

class Speed extends React.Component {

    render() {
        return(
            <div>
                <Card>
            <CardHeader>
              <h5 className="title">Speed Unit</h5>
            </CardHeader>
            <CardBody className="all-icons">
            
            <Form className="border rounded p-2 my-2">
              <FormGroup className="d-inline-block mr-5" check>
                <Label check>
                  <Input type="radio" name="speed" defaultChecked/>
                  m/s
                </Label>
                </FormGroup>
                <FormGroup className="d-inline-block mr-5" check>
                <Label check>
                  <Input type="radio" name="speed"/>
                  km/hour
                </Label>
              </FormGroup>
              <FormGroup className="d-inline-block mr-5" check>
                <Label check>
                    <Input type="radio" name="speed" />
                  knot
                </Label>
              </FormGroup>
              <FormGroup className="d-inline-block mr-5" check>
                  <Label check>
                      <Input type="radio" name="speed" />
                 mile/hour
                  </Label>
              </FormGroup>
            </Form>
            
            </CardBody>
          </Card>
            </div>
        );
    }
}

export default Speed;