import React from 'react';
import {Card, CardHeader, CardBody, Form, FormGroup, Label, Input} from 'reactstrap';

class Altitude extends React.Component {
    render() {
        return (

            <div>
            <Card>
            <CardHeader>
              <h5 className="title">Altitude Unit</h5>
            </CardHeader>
            <CardBody className="all-icons">
            
            <Form className="border rounded p-2 my-2">
              <FormGroup className="d-inline-block mr-5" check>
                <Label check>
                  <Input type="radio" name="altitude" defaultChecked/>
                  meter
                </Label>
                </FormGroup>
                <FormGroup className="d-inline-block mr-5" check>
                <Label check>
                  <Input type="radio" name="altitude"/>
                  feet
                </Label>
              </FormGroup>
            </Form>
            
            </CardBody>
          </Card>
          </div>  
        );
    }
}

export default Altitude;