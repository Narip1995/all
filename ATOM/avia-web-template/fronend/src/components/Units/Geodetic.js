import React from 'react';
import { Card,
         CardHeader,
         CardBody,
         Form, 
         FormGroup, 
         Label,
         Input, 
         CustomInput, 
         InputGroup,
         InputGroupAddon } from 'reactstrap';

class Geodetic extends React.Component {
    render() {
        return(
            <div>
            <Card>
                <CardHeader>
                  <h5 className="title">Geodetic</h5>
                </CardHeader>
                <CardBody className="all-icons">
                
                <Form className="border rounded p-2 my-2" >
                        <h6>Zone</h6>

                        <FormGroup className="d-inline-block mr-5" check>
                            <Label check>
                              <Input type="checkbox" name="show-zone" />
                              N/A 
                            </Label>
                        </FormGroup>
                        <FormGroup className="d-inline-block mr-5" check>
                          <Label check>
                              <Input type="radio" name="zone" defaultChecked/>
                            FW
                          </Label>
                        </FormGroup>
                        <FormGroup className="d-inline-block mr-5" check>
                          <Label check>
                            <Input type="radio" name="zone"/>
                            BW
                          </Label>
                        </FormGroup>
                </Form>
                <Form className="border rounded p-2 my-2" >
                        <h6>Symbol</h6>

                        <FormGroup className="d-inline-block mr-5" check>
                            <Label check>
                              <Input type="checkbox" name="symbol" />
                              XX° XX' XX"
                            </Label>
                        </FormGroup>
                    
                </Form>
                <Form className="border rounded p-2 my-2" >
                        <h6>Format</h6>

                        <FormGroup className="d-block mr-5" check>
                            <Label check>
                              <Input type="radio" name="format" defaultChecked/>
                            DD 
                            </Label>
                        </FormGroup>
                        <FormGroup className="d-block mr-5" check>
                          <Label check>
                              <Input type="radio" name="format" />
                            DD MM SS
                          </Label>
                        </FormGroup>
                        <FormGroup className="d-block mr-5" check>
                          <Label check>
                            <Input type="radio" name="format"/>
                            DD MM.SS
                          </Label>
                        </FormGroup>
                </Form>
                </CardBody>
            </Card>
            </div>
        );
    }
}

export default Geodetic;