import React from 'react';
import {Card, CardHeader, CardBody, Form, FormGroup, Label, Input } from 'reactstrap';

class Distance extends React.Component {

    render() {
        return (
            <div>
            <Card>
            <CardHeader>
              <h5 className="title">Distance Unit</h5>
            </CardHeader>
            <CardBody className="all-icons">
            
            <Form className="border rounded p-2 my-2">
              <FormGroup className="d-inline-block mr-5" check>
                <Label check>
                  <Input type="radio" name="distance" defaultChecked/>
                  meter
                </Label>
                </FormGroup>
                <FormGroup className="d-inline-block mr-5" check>
                <Label check>
                  <Input type="radio" name="distance"/>
                  kilometer
                </Label>
              </FormGroup>
              <FormGroup className="d-inline-block mr-5" check>
                <Label check>
                    <Input type="radio" name="distance" />
                  yard
                </Label>
              </FormGroup>
              <FormGroup className="d-inline-block mr-5" check>
                  <Label check>
                      <Input type="radio" name="distance" />
                 nautical mile
                  </Label>
              </FormGroup>
            </Form>
            
            </CardBody>
          </Card>
          </div> 
        );
    }
}

export default Distance