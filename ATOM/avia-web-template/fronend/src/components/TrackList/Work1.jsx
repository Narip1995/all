import React from "react"
import Work_Enemy from "./Data/Work_Enemy"
import Work_Total from "./Data/Work_Total"
import { TabContent, TabPane } from "reactstrap"
// import '../../../css/AnimatedRadioTab.css'

export default class Work extends React.Component {
    state = {
        activeTab: '1'
    };
    toggle = (tab) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    render() {
        return (
            <div className="container">

                <div className="btn-group">
                    <a className="btn btn-secondary" data-toggle="collapse" href="#collapseExample" role="button"
                        aria-expanded="false" aria-controls="collapseExample">collapse</a>
                    <div className="btn-group dropright" role="group">
                        <button type="button" className="btn-sm btn btn-secondary dropdown-toggle dropdown-toggle-split"
                            id="ToggleMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        </button>
                        <div className="dropdown-menu" aria-labelledby="ToggleMenu">
                            <a className="dropdown-item" href="#" data-toggle="collapse" href="#collapseExample" onClick={() => { this.toggle('1'); }}>Total</a>
                            <a className="dropdown-item" href="#" data-toggle="collapse" href="#collapseExample" onClick={() => { this.toggle('2'); }}>Enemy</a>
                            <a className="dropdown-item" href="#">Friend</a>
                            <a className="dropdown-item" href="#">Unknown</a>
                            <a className="dropdown-item" href="#">Arthur</a>
                            <a className="dropdown-item" href="#">G180</a>
                        </div>
                    </div>
                </div>

                <div className="collapse" id="collapseExample">
                    <div className="card card-body">
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <Work_Total />
                            </TabPane>
                        </TabContent>

                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="2">
                                <Work_Enemy />{' '}
                            </TabPane>
                        </TabContent>
                        <table className="table">
                            <thead>
                                <tr>
                                    <th>
                                        HelloWork
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>



            </div>
        );
    }
}
//ปรับแก้และทดสอบ
