import React from "react";

import classnames from "classnames";
export default class Work_Enemy extends React.Component {
        
    render() {
        return (
      
            <div >
                <nav className="navbar navbar-expand-lg navbar-light blue-grey lighten-5 mb-4 ">
                        <ul className="nav nav-pills " role="tablist">
                            <li className="nav-item ">
                                <a className="nav-link active" id="pills-Enemy" data-toggle="pill" href="pills-Enemy">Track Enemy</a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link" id="pills-Category" data-toggle="pill" href="pills-Category">Category</a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link" id="pills-Position" data-toggle="pill" href="pills-Position" href="#">Position</a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link" id="pills-Altitude" data-toggle="pill" href="pills-Altitude" href="#">Altitude</a>
                            </li>
                            <li className="nav-item ">
                                <a className="nav-link" id="pills-Speed" data-toggle="pill" href="pills-Speed" href="#">Speed</a>
                            </li>
                        </ul>

                        <form className="form-inline">
                            <input className="form-control" type="text" placeholder="Search" aria-label="Search" />
                        </form>

                </nav>
            </div>
        );
    }
}