import Dashboard from "./views/Dashboard.jsx";
import Icons from "views/Icons.jsx";
import Map from "views/Map.jsx";
import Notifications from "views/Notifications.jsx";
import Rtl from "views/Rtl.jsx";
import TableList from "views/TableList.jsx";
import Typography from "views/Typography.jsx";
import UserProfile from "views/UserProfile.jsx";
import Unit from "views/Unit.jsx";
import Setting from "views/Setting.jsx"
import Tracks from "./views/Tracks"


var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "tim-icons icon-chart-pie-36",
    component: Dashboard,
    layout: "/admin"
  },
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   icon: "tim-icons icon-atom",
  //   component: Icons,
  //   layout: "/admin"
  // },
  // {
  //   path: "/tracks",
  //   name: "Tracks",
  //   icon: "tim-icons icon-atom",
  //   component: Tracks,
  //   layout: "/admin"
  // },
  {
    path: "/map",
    name: "Map",
    icon: "tim-icons icon-world",
    component: Map,
    layout: "/admin"
  },
  // {
  //   path: "/unit",
  //   name: "Unit",
  //   icon: "tim-icons icon-settings",
  //   component: Unit,
  //   layout: "/admin"
  // },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   icon: "tim-icons icon-bell-55",
  //   component: Notifications,
  //   layout: "/admin"
  // },
  // {
  //   path: "/user-profile",
  //   name: "User Profile",
  //   icon: "tim-icons icon-single-02",
  //   component: UserProfile,
  //   layout: "/admin"
  // },
  {
    path: "/setting",
    name: "Setting",
    icon: "tim-icons icon-settings-gear-63",
    component: Setting,
    layout: "/admin"
  }

];
export default routes;
