import React from "react";
// reactstrap components
import { Card, CardHeader, CardBody, Row, Col, CardImg, Button } from "reactstrap";

import Altitude from "../components/Units/Altitude"
import Distance from "../components/Units/Distance"
import Speed from "../components/Units/Speed"
import Geodetic from "../components/Units/Geodetic"
import AviaImg from '../assets/img/Logo - Avia Satcom.png'

class Setting extends React.Component {

  constructor() {
    super();
    this.state = {
      config: require('../configuration.json').configuration,
      language: require('../configuration.json').language,
      default: require('../configuration.json').language.default
    };
   }

  componentDidMount() {
    this.readConfig();
  }

  readConfig() {
    const config = require('../configuration.json').configuration;
    const language = require('../configuration.json').language;
    const defaultLang = require('../configuration.json').language.default;

    this.setState({
      config: config,
      language: language,
      default: defaultLang
    })
  }

  render() {
    return (
      <div className="content">
      <Row>
        <Col md="4">
          <Altitude/>
          <Distance/>
          <Speed/>
          <Geodetic/>
        </Col>

        <Col md="4">
          <Card>
            <CardHeader>
              <h5 className="title">Configuration</h5>
              
            </CardHeader>
            <CardBody className="all-icons">
              <h5>Debug : {this.state.config.debug}</h5>
              <h5>Window : </h5>
              <ul>
              <li>Width : {this.state.config.window.width}</li>
              <li>Height : {this.state.config.window.height}</li>
              </ul>
              
              <h5>Server : </h5>
              <ul>
              <li>Name : {this.state.config.server.name}</li>
              <li>Path : {this.state.config.server.path}</li>
              <li>Base : {this.state.config.server.base}</li>
              </ul>
              <h5>Map : </h5>
              <ul>
              <li>Path : {this.state.config.maps.path}</li>
              <li>Delay : {this.state.config.maps.delay}</li>
              <li>Render : {this.state.config.maps.render}</li>
              <li>Display Mode : {this.state.config.maps.display_mode}</li>
              </ul>
            </CardBody>
          </Card>
        </Col>

        <Col md="4">
          <Card>
            <CardHeader>
              <h5 className="title">Language : {this.state.language[this.state.default].label}</h5>
              
            </CardHeader>
            <CardBody className="all-icons">
              <h5>{this.state.language[this.state.default].windows.label} : </h5>
              <ul>
                <li>{this.state.language[this.state.default].windows.compass}</li>
                <li>{this.state.language[this.state.default].windows.position}</li>
                <li>{this.state.language[this.state.default].windows.error}</li>
                <li>{this.state.language[this.state.default].windows.time}</li>
              </ul>
              <h5>{this.state.language[this.state.default].main_display.label} : </h5>
              <ul>
                <li>{this.state.language[this.state.default].main_display.trackData}</li>
                <li>{this.state.language[this.state.default].main_display.trackList}</li>
                <li>{this.state.language[this.state.default].main_display.trackInfo}</li>
                <li>{this.state.language[this.state.default].main_display.displayOption}</li>
              </ul>
              <Button onClick={()=>{this.setState({default : "thai"})}}>
                {this.state.language.thai.label}
              </Button>
              <Button onClick={()=>{this.setState({default : "english"})}} >
                {this.state.language.english.label}
              </Button>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
    );
  }
}

export default Setting;
