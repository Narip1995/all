import React from "react";
import axios from 'axios';

// reactstrap components
import { Card, CardHeader, CardBody, Row, Col, CardImg, Button } from "reactstrap";
import AviaImg from '../assets/img/Logo - Avia Satcom.png'


class Tracks extends React.Component {

  constructor() {
    super();
    this.state = {
      config: require('../configuration.json').configuration,
      language: require('../configuration.json').language
    };
   }

  componentWillMount() {
    this.readConfig();
  }

  readConfig() {
    console.log("config state : ", this.state.config)
    console.log("language state : ", this.state.language)
  }
  


  render() {
    return (
      <div className="content">
      <Row>
        <Col md="6">
          <Card>
            <CardHeader>
              <h5 className="title">Configuration</h5>
              
            </CardHeader>
            <CardBody className="all-icons">
              <h5>Debug : {this.state.config.debug}</h5>
              <h5>Window </h5>
              <ul>
              <li>Width : {this.state.config.window.width}</li>
              <li>Height : {this.state.config.window.height}</li>
              </ul>
              
              <h5>Server : </h5>
              <ul>
              <li>Name : {this.state.config.server.name}</li>
              <li>Path : {this.state.config.server.path}</li>
              <li>Base : {this.state.config.server.base}</li>
              </ul>
              <h5>Map </h5>
              <ul>
              <li>Path : {this.state.config.maps.path}</li>
              <li>Delay : {this.state.config.maps.delay}</li>
              <li>Render : {this.state.config.maps.render}</li>
              <li>Display Mode : {this.state.config.maps.display_mode}</li>
              </ul>
            </CardBody>
          </Card>
        </Col>

        <Col md="6">
          <Card>
            <CardHeader>
              <h5 className="title">Language</h5>
              
            </CardHeader>
            <CardBody className="all-icons">
              <Button>
                {this.state.language.thai.label}
              </Button>
              <Button>
                {this.state.language.english.label}
              </Button>
            </CardBody>
          </Card>
        </Col>
      </Row>
    </div>
    );
  }
}

export default Tracks;
