import React from "react";
// reactstrap components
import { Card, CardHeader, CardBody, Row, Col, CardImg, Button } from "reactstrap";

import Altitude from "../components/Units/Altitude"
import Distance from "../components/Units/Distance"
import Speed from "../components/Units/Speed"
import Geodetic from "../components/Units/Geodetic"
import LanguageForm from '../components/Forms/LanguageForm'
import JsonForm from '../components/Forms/JsonForm'
import AviaImg from '../assets/img/Logo - Avia Satcom.png'

class Setting extends React.Component {



  render() {
    return (
      <div className="content">
      <Row>
        <Col md="4">
          <Altitude/>
          <Distance/>
          <Speed/>
          <Geodetic/>
        </Col>

        <Col md="4">
          <JsonForm/>
        </Col>

        <Col md="4">
          <LanguageForm/>
        </Col>
      </Row>
    </div>
    );
  }
}

export default Setting;
