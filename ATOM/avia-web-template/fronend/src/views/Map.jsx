import React from "react";

// reactstrap components
import { Card, CardHeader, CardBody, Row, Col, CardImg } from "reactstrap";
import AviaImg from '../assets/img/Logo - Avia Satcom.png'

class Map extends React.Component {
  render() {
    return (
      
        <div className="content">
          <Row>
            <Col md="12">
              <Card>
                <CardHeader>
                  <h5 className="title">All new C2</h5>
                  <p className="category">
                    Created by Software Team from{" "}
                    <a href="https://www.google.com">AVIA SATCOM</a>
                  </p>
                </CardHeader>
                <CardBody className="all-icons">
                  <CardImg width="100%" src={AviaImg}/>
                </CardBody>
              </Card>
            </Col>
          </Row>
        </div>
      
    );
  }
}

export default Map;
