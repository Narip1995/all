import React from "react";
// nodejs library that concatenates classes
import classNames from "classnames";
// react plugin used to create charts
import { Line, Bar } from "react-chartjs-2";

// reactstrap components
import {
  Button,
  ButtonGroup,
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
  Label,
  FormGroup,
  Input,
  Table,
  Row,
  Col,
  UncontrolledTooltip
} from "reactstrap";

// core components
// import {chartExample4} from "variables/charts.jsx";
import ChartExample4 from '../variables/charts.jsx'

class Rtl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bigChartData: "data1"
    };
  }
  setBgChartData = name => {
    this.setState({
      bigChartData: name
    });
  };
  render() {
    return (
      <>
        <div className="content">
          <Row>
            <Col className="text-right" lg="4">
              <Card className="card-chart">
             
                <CardBody>
                  <div className="chart-area">
                    {/* <Line
                      // data={chartExample4.data}
                      // options={chartExample4.options}
                    /> */}
                    <ChartExample4/>
                  </div>
                </CardBody>
              </Card>
            </Col>
          </Row>
         
        </div>
      </>
    );
  }
}

export default Rtl;
