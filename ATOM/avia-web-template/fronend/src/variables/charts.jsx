import React from 'react'
import { Line } from 'react-chartjs-2'
import database from '../apis/config'

const chartExample4 = {
  trackInfos: [],
  friends: [],
  enemies: [],
  nutrals: [],
  unknowns: [],
  options: {
    maintainAspectRatio: false,
    legend: {
      display: false
    },
    responsive: true,
    scales: {
      yAxes: [
        {
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: "rgba(29,140,248,0.0)",
            zeroLineColor: "transparent"
          },
          ticks: {
            suggestedMin: 2,
            suggestedMax: 10,
             padding: 10,
            fontColor: "#9e9e9e"
          }
        }
      ],
      xAxes: [
        {
          barPercentage: 1.6,
          gridLines: {
            drawBorder: false,
            color: "rgba(0,242,195,0.1)",
            zeroLineColor: "transparent"
          },
          ticks: {
            padding: 20,
            fontColor: "#9e9e9e"
          }
        }
      ]
    }
  },
  data: {
    labels: ["Friend", "Enemy", "Nutral", "Unknown"],
    datasets: [
      {
        label: "Identity",
        fill: true,
        backgroundColor: "rgba(29,140,248,0.4)",
        borderColor: "#1f8ef1",
        borderWidth: 2,
        borderDash: [],
        borderDashOffset: 0.0,
        pointBackgroundColor: "#1f8ef1",
        pointBorderColor: "rgba(255,255,255,0)",
        pointHoverBackgroundColor: "#1f8ef1",
        pointBorderWidth: 20,
        pointHoverRadius: 4,
        pointHoverBorderWidth: 15,
        pointRadius: 4,
        data: []
      }
    ]
  }
};


export default class asd extends React.Component {

  state = chartExample4;
  componentDidMount = () => {
    this.fetchTrackInfo()
  }

  async fetchTrackInfo() {
    const response = await database.get('/trackInfos');
    const trackInfos = await response.data;
    console.log(trackInfos)
    const friends = await this.trackIdentities(trackInfos, "Friend");
    const enemies = await this.trackIdentities(trackInfos, "Enemy");
    const nutrals = await this.trackIdentities(trackInfos, "Nutral");
    const unknowns = await this.trackIdentities(trackInfos, "Unknown");
    const data = this.state.data;
    data.datasets[0].data = [friends.length, enemies.length, nutrals.length, unknowns.length]
    await this.setState(prevState => ({
      ...prevState,
      friends: friends,
      enemies: enemies,
      nutrals: nutrals,
      unknowns: unknowns,
      trackInfos: [...trackInfos],
      data: data
    }))
    console.log(this.state)
  }

  trackIdentities(trackInfos, identity) {
    return trackInfos.filter(track => {
      return track.identity.toLowerCase().includes(identity.toLowerCase());
    })
  }

  render() {

    return (<div>
      <Line data={this.state.data} options={this.state.options} />

    </div>
    );

  }

}




