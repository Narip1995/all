import React from 'react'
import MovingModal from './MovingModal'
import Modaless from './Modaless'
import ModalsLauncher from './ModalsLauncher'
import '../css/AnimatedRadioTab.css'
import '../css/TestCollapse.css'
import '../css/AccordionMenu.css'

import Work1 from './work/TrackList/Work1'
import PostForm from './work/configuration/PostForm'

export default class App extends React.Component {
    render() {
        return (
            <div>
                <ModalsLauncher /> 
                <Work1/> 
            </div>
        );
    }
}
