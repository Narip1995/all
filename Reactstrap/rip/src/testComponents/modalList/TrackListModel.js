import React from 'react';
import TrackList from './TrackList';
import TrackList2 from './TrackList2';

import $ from 'jquery/dist/jquery';
import '../../css/trackModal.css';


import '../../css/AccordionMenu.css';
// import AnimatedRedioTab from './AnimatedRedioTab';
import AccordionMenu from './AccordionMenu';
// import Test from './Test';


export default class TrackListModal extends React.Component {

  componentDidMount() {
    $("#addTrackList").draggable({
      handle: ".modal-header"
    });
    $('.modal').modal({
      backdrop: false,
      show: false
    });
  }

  render() {
    return (
      <div className="modal fade modeless" id="addTrackList">
        <div className="modal-dialog modal-lg" >
          <div className="modal-content">
            <div className="modal-header bg-primary text-white">
              <h5 className="modal-title">Track List</h5>
              <button className="close" data-dismiss="modal">
                <span>&times;</span>
              </button>
            </div>
            <div className="modal-body">
            <AccordionMenu/>
            </div>
            <div className="modal-footer">
              <button className="btn btn-danger" data-dismiss="modal">Save Changes</button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}