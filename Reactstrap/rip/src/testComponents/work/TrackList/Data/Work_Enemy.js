import React from 'react';
import classnames from 'classnames';
export default class Work_Enemy extends React.Component {
        
    render() {
        return (
      
            <div >
                <nav class="navbar navbar-expand-lg navbar-light blue-grey lighten-5 mb-4 ">
                        <ul class="nav nav-pills " role="tablist">
                            <li class="nav-item ">
                                <a class="nav-link active" id="pills-Enemy" data-toggle="pill" href="pills-Enemy">Track Enemy</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" id="pills-Category" data-toggle="pill" href="pills-Category">Category</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" id="pills-Position" data-toggle="pill" href="pills-Position" href="#">Position</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" id="pills-Altitude" data-toggle="pill" href="pills-Altitude" href="#">Altitude</a>
                            </li>
                            <li class="nav-item ">
                                <a class="nav-link" id="pills-Speed" data-toggle="pill" href="pills-Speed" href="#">Speed</a>
                            </li>
                        </ul>

                        <form class="form-inline">
                            <input class="form-control" type="text" placeholder="Search" aria-label="Search" />
                        </form>

                </nav>
            </div>
        );
    }
}