import React from 'react'
import Work_Enemy from './Data/Work_Enemy'
import Work_Total from './Data/Work_Total'
import { TabContent, TabPane, Table } from 'reactstrap'

export default class Work extends React.Component {
    state = {
        activeTab: '1'
    };
    toggle = (tab) => {
        if (this.state.activeTab !== tab) {
            this.setState({
                activeTab: tab
            });
        }
    }
    render() {
        return (
            <div class="container">

                <div class="btn-group">
                    <a class="btn btn-secondary" data-toggle="collapse" href="#collapseExample" role="button"
                        aria-expanded="false" aria-controls="collapseExample">collapse</a>
                    <div class="btn-group dropright" role="group">
                        <button type="button" class="btn-sm btn btn-secondary dropdown-toggle dropdown-toggle-split"
                            id="ToggleMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        </button>
                        <div class="dropdown-menu" aria-labelledby="ToggleMenu">
                            <a class="dropdown-item" href="#" data-toggle="collapse" href="#collapseExample" onClick={() => { this.toggle('1'); }}>Total</a>
                            <a class="dropdown-item" href="#" data-toggle="collapse" href="#collapseExample" onClick={() => { this.toggle('2'); }}>Enemy</a>
                            <a class="dropdown-item" href="#">Friend</a>
                            <a class="dropdown-item" href="#">Unknown</a>
                            <a class="dropdown-item" href="#">Arthur</a>
                            <a class="dropdown-item" href="#">G180</a>
                        </div>
                    </div>
                </div>

                <div class="collapse" id="collapseExample">
                    <div class="card card-body">
                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="1">
                                <Work_Total />
                            </TabPane>
                        </TabContent>

                        <TabContent activeTab={this.state.activeTab}>
                            <TabPane tabId="2">
                                <Work_Enemy />{' '}
                            </TabPane>
                        </TabContent>

                        <Table size="sm">
                            <thead>
                                <tr>
                                    <th>Track No</th>
                                    <th>Category </th>
                                    <th >Position <a className="text-danger">UTM</a></th>
                                    <th>Altitude</th>
                                    <th>Speed <a className="text-danger">m/n</a></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Call</td>
                                    <td>Call</td>
                                    <td>Call</td>
                                    <td>Call</td>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Call</td>
                                    <td>Call</td>
                                    <td>Call</td>
                                    <td>Call</td>
                                </tr>
                                <tr>
                                    <th scope="row">1</th>
                                    <td>Call</td>
                                    <td>Call</td>
                                    <td>Call</td>
                                    <td>Call</td>
                                </tr>
                            </tbody>
                        </Table>
                    </div>
                </div>



            </div>
        );
    }
}
//ปรับแก้และทดสอบ
