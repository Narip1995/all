import React from 'react';
import axios from 'axios';
// import { Redirect } from 'react-router-dom';
// import { NumberFormat, Text } from 'react';
// import { Link } from 'react-router-dom';
import { Fade } from 'reactstrap';

import './GroupButtons_Language.css';

export default class CourseShow extends React.Component {

    state = {
        tracks: require('./language.json').language,
        visibleThail: true
    }
    componentDidMount() {
        this.readConfig();
    }
    readConfig = () => {
        console.log("config state:", this.state.language)
        //  length = jsonObj.length(language);
    }
    ToggleThail = () => {
        this.setState({
            visibleThail: !this.state.visibleThail
        });
    }
    ToggleEnglish = () => {
        this.setState({
            visibleEnglish: !this.state.visibleEnglish
        });
    }
    render() {


        return (
            <div>
                <div class="container" >
                    <div class="row">
                        <div class="col-md-12">
                            <div class="ui-group-buttons">
                                <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="btn btn-danger nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true"><spen class="glyphicon glyphicon-thumbs-up">TH</spen></a>
                                    </li>
                                    <div class="or"></div>
                                    <li class="nav-item">
                                        <a class="btn btn-success nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false"><spen class="glyphicon glyphicon-thumbs-down">EN</spen></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                        <h1 class="text-danger">{this.state.tracks.thail.label}</h1>
                        <h4><label class="text-danger">TrackData</label>{' '}{this.state.tracks.thail.windows.trackData}</h4>
                        <h4><label class="text-danger">TrackList</label>{' '}{this.state.tracks.thail.windows.trackList}</h4>
                        <h4><label class="text-danger">TrackInfo</label>{' '}{this.state.tracks.thail.windows.trackInfo}</h4>
                        <h4><label class="text-danger">TrackInfo</label>{' '}{this.state.tracks.thail.windows.displayOption}</h4>
                        <h4><label class="text-danger">Compass</label>{' '}{this.state.tracks.thail.main_display.compass}</h4>
                        <h4><label class="text-danger">Position</label>{' '}{this.state.tracks.thail.main_display.position}</h4>
                        <h4><label class="text-danger">Error</label>{' '}{this.state.tracks.thail.main_display.error}</h4>
                        <h4><label class="text-danger">Time</label>{' '}{this.state.tracks.thail.main_display.time}</h4>
                    </div>
                    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                        <h1 class="text-success">{this.state.tracks.english.label}</h1>
                        <h4><label class="text-success">TrackList</label>{' '}{this.state.tracks.english.main_display.trackList}</h4>
                        <h4><label class="text-success">TrackData</label>{' '}{this.state.tracks.english.main_display.trackData}</h4>
                        <h4><label class="text-success">TrackInfo</label>{' '}{this.state.tracks.english.main_display.trackInfo}</h4>
                        <h4><label class="text-success">TrackInfo</label>{' '}{this.state.tracks.english.main_display.displayOption}</h4>
                        <h4><label class="text-success">Compass</label>{' '}{this.state.tracks.english.windows.compass}</h4>
                        <h4><label class="text-success">Position</label>{' '}{this.state.tracks.english.windows.position}</h4>
                        <h4><label class="text-success">Error</label>{' '}{this.state.tracks.english.windows.error}</h4>
                        <h4><label class="text-success">Time</label>{' '}{this.state.tracks.english.windows.time}</h4>
                    </div>
                </div>
            </div>
        );
    }
}