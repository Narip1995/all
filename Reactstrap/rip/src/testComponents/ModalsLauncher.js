import React from 'react'

import TrackListModal from './modalList/TrackListModel'
import TrackDataModal from './modalData/TrackDataModal'

class ModalsLauncher extends React.Component {
    render() {
        return(
            <div>
            <header id="main-header" className="py-2 bg-primary text-white">
              <div className="container">
                <div className="row">
                  <div className="col-md-6">
                    <h1>
                      <i className="fas fa-cog"></i>C2 Dialogs</h1>
                  </div>
                </div>
              </div>
            </header>
            <section id="actions" className="py-4 mb-4 bg-light">
              <div className="container">
                <div className="row">
                  <div className="col-md-3">
                    <a href="#" className="btn btn-primary btn-block" data-toggle="modal" data-target="#addTrackList">
                      <i className="fas fa-plus"></i> TrackList
                    </a>
                  </div>
                  <div className="col-md-3">
                    <a href="#" className="btn btn-success btn-block" data-toggle="modal" data-target="#addTrackData">
                      <i className="fas fa-plus"></i> TrackData
                    </a>
                  </div>
                  <div className="col-md-3">
                    <a href="#" className="btn btn-warning btn-block" data-toggle="modal" data-target="#addTrackList">
                      <i className="fas fa-plus"></i> Test
                    </a>
                  </div>
                </div>
              </div>
            </section>
            <TrackListModal/>
            <TrackDataModal/>
        </div>
        );
    }
}
export default ModalsLauncher;