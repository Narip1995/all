import React from 'react';
import { FormGroup, Label, Col } from 'reactstrap';
import { AvForm, AvField } from 'availity-reactstrap-validation';
import $ from 'jquery/dist/jquery.js';

export default class DataInformation extends React.Component {
  componentDidMount() {
    $(document).ready(function () {
      $('#TrackNo,#Speed,#Alt,#Bearing,#Position,#Category,#Platform').keydown(function (event) {
        if ($.inArray(event.keyCode, [8, 9, 27, 13, 46, 109, 110, 190, 189, 116]) !== -1 ||
        (((event.keyCode == 65 )||(event.keyCode == 67)) && event.ctrlKey === true) ||
          (event.keyCode >= 35 && event.keyCode <= 39) ) {
          return;
        }
        
        else {
          if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) 
          {
            event.preventDefault();
          }
          if(event.AltKey === false){
          event.preventDefault();
        }
        }
        
      });
      /*
      $('#TrackNo,#Speed,#Alt,#Bearing,#Position,#Category,#Platform').change(function () {
        var value = $(this).val();
        if (isNaN(value)) {
          $(this).val('0');
          alert('Require Number!');
          $(this).focus();
        }
      });*/
    });
  }

  render() {
    let max = 10, min = 4
    return (
      <AvForm >
        <div className="align-items-start">
          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>TrackNo</h6></Label>
            <Col>
              <AvField id="TrackNo" name="TrackNo" className="form-control-sm col-sm-12 col-md-12 " type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col>
             <div className="col-2">{/*<small iclass="text-muted">asdfsadf</small>*/}</div> 
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>Speed</h6></Label>
            <Col >
              <AvField id="Speed" name="Speed" className="form-control-sm col-sx-sm-12 col-md-12" type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col>
            <div className="col-2"><h6>m/s</h6></div>
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>Alt</h6></Label>
            <Col >
              <AvField id="Alt" name="Alt" className="form-control form-control-sm col-sx-sm-12 col-md-12" type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col>
            <div className="col-2"><h6>m</h6></div>
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>Bearing</h6></Label>
            <Col >
              <AvField id="Bearing" name="Bearing" className="form-control form-control-sm col-sx-sm-12 col-md-12" type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col>
            <div className="col-2"><h6>deg</h6></div>
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>Position</h6></Label>
            <Col >
              <AvField id="Position" name="Position" className="form-control form-control-sm col-sm-12 col-md-12" type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col>
            <div className="col-2"></div>
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>Category</h6></Label>
            <Col >
              <AvField id="Category" name="Category" className="form-control form-control-sm col-sm-12 col-md-12" type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col>
            <div className="col-2"></div>
          </FormGroup>

          <FormGroup row>
            <Label className="col-4 align-items-start"><h6>Platform</h6></Label>
            <Col >
              <AvField id="Platform" name="Platform" className="form-control form-control-sm col-sm-12 col-md-12" type="Input"
                validate={{ maxLength: { value: max }, minLength: { value: min } }} />
            </Col>
            <div className="col-2"></div>
          </FormGroup>
        </div>
      </AvForm>
    );
  }
}
