import React from 'react';

import 'bootstrap/dist/js/bootstrap.js';
import $ from 'jquery/dist/jquery.js';

class Modaless extends React.Component {

    state = {
        isDialogOpen: false,
        isDialogModal: false
    }

    onClose = () => {
        this.setState({ isDialogOpen: false });
    }

    onOpen = () => {
        this.setState({ isDialogOpen: true });
    }

    componentDidMount() {
        $("#myModal").draggable({
            handle: ".modal-header"
        });
    }

    render() {
        return (
            <div>
                <button className="btn btn-outline-primary" onClick={this.onOpen}>Open</button>
                <div class="side-menu" id="sideMenu">
                    <menu>
                        <ul class="nav nav-tabs nav-stacked">
                            <li><a href="#myModal" data-backdrop="false" data-toggle="modal">Click Me</a>
                            </li>
                            <li><a id="myAlert">Alert</a>
                            </li>
                        </ul>
                    </menu>
                </div>
                <div id="myModal" class="modal fade modeless">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title">Settings</h4>
                            </div>
                            <div class="modal-body">
                                <p>Settings</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Modaless;